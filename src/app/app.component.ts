import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'tests-app';

  //First test vars
  primary_array: any[]=[];
  temp_array: any[]=[];

  frmRows = this._fb.group({
    total_rows: ['', [Validators.required]]
  });

  //Second test vars
  selected_articles: any[]=[];
  articles = [
    {
      name: 'Sudadera',
      price: '800'
    },
    {
      name: 'Pants',
      price: '500'
    },
    {
      name: 'Short',
      price: '300'
    },
    {
      name: 'Chamarra',
      price: '1500'
    },
    {
      name: 'Playera',
      price: '28'
    }
  ];

  constructor(private _fb: UntypedFormBuilder){}

  //First test methods
  get validTotalRows(){
    return this.frmRows.get('total_rows')?.touched && this.frmRows.get('total_rows')?.invalid;
  }

  runRows(){
    if(this.frmRows.invalid){
      Object.values(this.frmRows.controls).forEach(control => {
        (control instanceof UntypedFormGroup) ? Object.values(control.controls).forEach(control => control.markAsTouched() ) : control.markAsTouched();
      });
    }else{

      let initial = 1;

      for(let i = 1; i <= this.frmRows.getRawValue().total_rows; i++){
        this.temp_array.push(initial);
        initial = initial + 2;
      }

      this.temp_array.map((e, index, array) => {
        this.primary_array.unshift(array.slice(0, array.length - index).reverse());
      });
      console.log(this.primary_array);
    }
  }

  resetRows(){
    this.frmRows.reset();
    this.primary_array=[];
    this.temp_array=[];
  }

  //Second test methods
  addArticle(article: any, index: number){
    this.selected_articles.push(article);
    this.articles.splice(index, 1);
  }

  rmArticle(article: any, index: number){
    this.articles.push(article);
    this.selected_articles.splice(index, 1);
  }

  makeOrder(){
    if(this.selected_articles.length < 3){
      Swal.fire({
        icon: 'error',
        title: 'Algo salio mal',
        html: 'Debe seleccionar al menos 3 artículos.',
        confirmButtonText: 'Entiendo',
        confirmButtonColor: '#616161',
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false
      });
    }else{
      Swal.fire({
        icon: 'success',
        title: 'Exito',
        html: 'Selección correcta.',
        confirmButtonText: 'Entiendo',
        confirmButtonColor: '#1676F1',
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false
      });
    }
  }


}
